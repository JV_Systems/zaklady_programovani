/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practice.two.cmd;

import java.util.Scanner;

/**
 *
 * @author borecnakonec
 */
public class PracticeTwoCmd {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double DPH = 0.21;

        // unit per kWh in czech crowns with DPH
        double pricePerUnit = 1200.45 * (1+DPH);
        System.out.println("unit price with DPH : " + pricePerUnit);

        // quantity of electricity per hour (in watts!!!)
        long quantity;
        System.out.println("Enter your quantity:");
        quantity = sc.nextLong();

        //result
        double billToPay = (((quantity*24*30)/1000)*pricePerUnit) * 0.046;

        System.out.println("Bill to pay : " + billToPay + " $ ");
        sc.close();
    }
    
}
