package cz.vsb.ekf.jam.one;

public class ExampleOne {

    public static void main(String[] args) {
        // quantity of electricity per hour (in watts!!!)
        int quantity = 280;
        
        // unit per kWh in czech crowns
        double pricePerUnit = 1200.45;

        //result
        double billToPay = ((quantity*24*30)/1000)*pricePerUnit ;

        //print to console
        System.out.println("Bill to pay : " + billToPay + " Kc");
    }
}
