package cz.vsb.ekf.jam.two;

import java.util.Scanner;

public class ExampleTwo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        final double DPH = 0.21;
        final int HOURS = 24;
        final int DAYS = 30;

        // unit per kWh in czech crowns with DPH
        double pricePerUnitWithDph = 1200.45 * (1+DPH);
        System.out.println("unit price with DPH : " + pricePerUnitWithDph);

        // quantity of electricity per hour (in watts!!!)
        long quantity;
        System.out.println("Enter your quantity:");
        quantity = sc.nextLong();

        //result
        double billToPay = (((quantity*HOURS*DAYS)/1000)*pricePerUnitWithDph) * 0.046;
        System.out.println("Bill to pay : " + billToPay + " $ ");
        
        sc.close();
    }
}
