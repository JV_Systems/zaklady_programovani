package cz.vsb.ekf.jam.three;

import java.util.Scanner;

public class ExampleThree {
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        final double DPH = 0.21;
        final int HOURS = 24;
        final int DAYS = 30;

        
        System.out.println("Enter unit price:");
        double unitPrice = sc.nextDouble();
        
        // unit price with/without DPH
        System.out.println("Do you want a price with DPH?");
        boolean withDPH = sc.nextBoolean();
        
        double resolvePriceUnit = withDPH ? unitPrice * (1+DPH) : unitPrice;
        System.out.println("Unit price : " + resolvePriceUnit + " Kc");

        // quantity of electricity per hour (in watts!!!)
        System.out.println("Enter your quantity:");
        long quantity = sc.nextLong();

        // result in dollars
        double billToPay = (((quantity*HOURS*DAYS)/1000)*resolvePriceUnit) * 0.046;
        System.out.println("Bill to pay : " + billToPay + " $ ");
        
        sc.close();
    }
}
