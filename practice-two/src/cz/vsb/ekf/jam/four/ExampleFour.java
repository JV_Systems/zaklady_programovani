package cz.vsb.ekf.jam.four;

import java.util.Scanner;

public class ExampleFour extends CalculateBill{

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        // quantity of electricity per hour (in watts!!!)
        System.out.println("Enter your quantity:");
        long quantity = sc.nextLong();

        // new instance of class
        CalculateBill bill = new CalculateBill();
        bill.compute(quantity);

        System.out.println("Bill to pay : " + bill.billToPay + " Kc");
        
        sc.close();
    }
}
