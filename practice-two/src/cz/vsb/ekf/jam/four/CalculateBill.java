package cz.vsb.ekf.jam.four;

public class CalculateBill {

    double billToPay;

    void compute(long quantity){
        // unit per kWh in czech crowns
        final double PRICE_UNIT = 1200.45;
        final int HOURS = 24;
        final int DAYS = 30;

        //result
        billToPay = ((quantity*HOURS*DAYS)/1000)*PRICE_UNIT ;
    }
}
