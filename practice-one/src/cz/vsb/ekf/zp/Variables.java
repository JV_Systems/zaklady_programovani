package cz.vsb.ekf.zp;

public class Variables {

    public static void main(String[] args) {
        int a = 2;
        double b = 3.1;
        float c = 3.1f;
        double result = a + b + c;
        
        boolean d = false;
        if(result > 100){
            d = true;
        }
        
        System.out.println("Is result bigger then 100? " + d);
        System.out.println(a + " + " + b + " + " + c + " = " + result);
    }
}
