package cz.vsb.ekf.zp;
public class Pojo {

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("no arguments, PANIC!");
            return;
        }

        String jmeno = (String) args[0];

        NovaTrida trida = new NovaTrida(jmeno);

        System.out.println("Hello, My name is " + trida.getJmeno() + ".");
    }

    public static class NovaTrida {

        private String jmeno;
        private String prijmeni;
        private String vek;

        public NovaTrida(String jmeno, String prijmeni, String vek) {
            this.jmeno = jmeno;
            this.prijmeni = prijmeni;
            this.vek = vek;
        }

        public NovaTrida(String jmeno) {
            this.jmeno = jmeno;
        }

        public NovaTrida() {
        }

        public String getJmeno() {
            return jmeno;
        }

        public void setJmeno(String jmeno) {
            this.jmeno = jmeno;
        }

        public String getPrijmeni() {
            return prijmeni;
        }

        public void setPrijmeni(String prijmeni) {
            this.prijmeni = prijmeni;
        }

        public String getVek() {
            return vek;
        }

        public void setVek(String vek) {
            this.vek = vek;
        }

        public void setCeleJmeno(String jmeno, String prijmeni) {
            this.jmeno = jmeno;
            this.prijmeni = prijmeni;
        }
    }

}
