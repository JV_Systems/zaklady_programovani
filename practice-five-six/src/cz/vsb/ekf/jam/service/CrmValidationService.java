package cz.vsb.ekf.jam.service;

import cz.vsb.ekf.jam.model.Person;
import cz.vsb.ekf.jam.model.SalesRepresentative;
import cz.vsb.ekf.jam.model.Sex;
import cz.vsb.ekf.jam.model.User;

public abstract class CrmValidationService {
    
    protected Person validateAllUsers(Person[] persons, String username, String password){
        for(Person p : persons){
            if(p.validateInputValues(username, password)){
                if(p instanceof SalesRepresentative){
                    System.out.println("=== WELCOME BACK BOSS===");
                    System.out.println("Successfully logged as " + p.getName() + " " + p.getSurname());
                } else if (p instanceof User){
                    System.out.println("Successfully logged as " + p.getName() + " " + p.getSurname());
                } else {
                    System.out.println("Type of person doesnt exists.");
                }
                
                return p;
            }
        }
        return null;
    }
    
    protected boolean isInSex(String value) {
        if(value != null){
            for (Sex s : Sex.values()) {
                if(s.name().equals(value)) { 
                    return true; 
                }
            }
        }
        return false;
    }
    
}
