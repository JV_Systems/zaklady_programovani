package cz.vsb.ekf.jam.service;

import cz.vsb.ekf.jam.model.Person;
import cz.vsb.ekf.jam.model.SalesRepresentative;
import cz.vsb.ekf.jam.model.Sex;
import cz.vsb.ekf.jam.model.User;
import java.math.BigDecimal;
import java.util.Scanner;

public class CrmService extends CrmValidationService{
   
    public CrmService(){
        
    }
    
    public void runApp(){
        Scanner sc = new Scanner(System.in);

        User user1 = new User(1, "David", "Jamarik", "SlepiciBujon23", "MojeHeslo123", 25, Sex.MALE, new BigDecimal("1500"));
        User user2 = new User(2, "Cristiano", "Ronaldo", "Cristi23", "MojeHeslo123", 25, Sex.TRANS, new BigDecimal("2500"));
        User user3 = new User(3, "Bara", "Votikova", "Barca23", "MojeHeslo123", 25, Sex.FEMALE, new BigDecimal("3500"));
        User user4 = new User(4, "Lionel", "Messi", "Leo23", "MojeHeslo123", 25, Sex.MALE, new BigDecimal("4500"));
        User user5 = new User(5, "Michal", "Krmencik", "Majkl0", "MojeHeslo123", 25, Sex.OTHER, new BigDecimal("5500"));
        
        SalesRepresentative sr1 = new SalesRepresentative(1, "boss", "Main", "sr", "MainSR", "123");
        SalesRepresentative sr2 = new SalesRepresentative(2, "basic", "Basic", "sr", "BasicSR", "1234");
        Person[] persons = {user1,user2,user3, user4, user5, sr1, sr2};

        System.out.println("=== Welcome here ===");
        System.out.println("Enter your username: ");
        String username = sc.next();

        System.out.println("Enter your password: ");
        String password = sc.next();

        Person loggedUser = validateAllUsers(persons, username, password);
        if(loggedUser == null){
            System.out.println("Invalid username or password");
            return;
        }
        
        System.out.println("===");
        System.out.println("===");
        System.out.println("===User dashboard===\n" + loggedUser);
        System.out.println("===");
        System.out.println("===");
        
        /*System.out.println("Do you want to change a sex? ");
        boolean change = sc.nextBoolean();
        
        if(change){
            System.out.println("Options: " + Arrays.asList(Sex.values()));
            System.out.println("===");
            System.out.println("===");
            
            System.out.println("Do you want to change a sex? ");
            String newSex = sc.next();
            
            if(isInSex(newSex)){
                loggedUser.setSex(Sex.valueOf(newSex));
                System.out.println(loggedUser);
            }
            return;
        }
        
        System.out.println("Please enter price to pay: ");
        double price = sc.nextDouble();

        if(new BigDecimal(price).compareTo(loggedUser.getPriceToPay()) == 1){
            System.err.println("The price is bigger than bill");
        } else if(new BigDecimal(price).compareTo(loggedUser.getPriceToPay()) == 0){
            System.out.println("The bill has been paid");
        } else {
            loggedUser.setPriceToPay(loggedUser.getPriceToPay().subtract(new BigDecimal(price)));
            System.out.println("Remaining amount to be paid => " + loggedUser.getPriceToPay() + " Kč <= ");
        }*/
    }
}
