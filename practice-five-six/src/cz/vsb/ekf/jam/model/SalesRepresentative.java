package cz.vsb.ekf.jam.model;

public class SalesRepresentative extends Person{
    private Integer id;
    private String position;

    public SalesRepresentative(Integer id, String position, String name, String surname, String username, String password) {
        super(name,surname,username,password);
        this.id = id;
        this.position = position;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "SalesRepresentative{ \n"
                + " name=" + getName() + ", \n"
                + " surname=" + getSurname() + ", \n"
                + " position=" + position + '}';
    }
    
    
}
