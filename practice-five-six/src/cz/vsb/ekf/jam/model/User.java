package cz.vsb.ekf.jam.model;

import java.math.BigDecimal;

public class User extends Person{
    private Integer id;
    private Integer age;
    private Sex sex;
    private BigDecimal priceToPay;

    public User(Integer id, String name, String surname, String username, String password, Integer age, Sex sex, BigDecimal priceToPay) {
        super(name,surname,username,password);
        this.id = id;
        this.age = age;
        this.sex = sex;
        this.priceToPay = priceToPay;
    }

    public User(Integer id, String name, String surname, String username, String password) {
        super(name,surname,username,password);
        this.id = id;
        this.priceToPay = new BigDecimal(Math.random() * 1000);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public BigDecimal getPriceToPay() {
        return priceToPay;
    }

    public void setPriceToPay(BigDecimal priceToPay) {
        this.priceToPay = priceToPay;
    }

    @Override
    public String toString() {
        return  "name = " + super.getName() + "\n" +
                "surname = " + super.getSurname() + "\n" +
                "sex = " + sex + "\n" +
                "price to pay = " + priceToPay + " Kč";
    }

    public void substractPriceToPay(BigDecimal price){
        this.setPriceToPay(this.priceToPay != null ? this.getPriceToPay().subtract(price) : null);
    }
    
}
