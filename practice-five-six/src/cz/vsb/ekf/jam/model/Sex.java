package cz.vsb.ekf.jam.model;

public enum Sex {
    MALE("popis1"), 
    FEMALE("popis2"), 
    TRANS("popis3"), 
    OTHER("popis4"), 
    ASEXUAL("popis5"), 
    ANDROGYN("popis6"), 
    AGENDER("popis7"), 
    BIGENDER("popis8");
    
    private String info;

    private Sex(String info) {
        this.info = info;
    }

    public String getInfo() {
        return info;
    }
}
