package cz.vsb.ekf.jam;

import cz.vsb.ekf.jam.service.CrmService;

public class Main {

    public static void main(String[] args) {
        CrmService app = new CrmService();
        app.runApp();
    }   
}
