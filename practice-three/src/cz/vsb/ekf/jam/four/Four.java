package cz.vsb.ekf.jam.four;

import java.util.Scanner;

public class Four {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter your text: ");
        String text = input.next();
        
        for(int i = 1; i <= 1000; i++){
            System.out.println(i + " " + text);
        }
    }
    
}
