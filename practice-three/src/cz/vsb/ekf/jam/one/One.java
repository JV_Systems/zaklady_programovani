
package cz.vsb.ekf.jam.one;

import java.util.Scanner;

public class One {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Enter your age: ");
        int age = sc.nextInt();
        
        if(age < 0){
            System.err.println("Not born yet!!!");
            return;
        } else if (age > 100){
            System.err.println("Too old!!!");
            return;
        }
        
        System.out.println("Enter your mood: ");
        String mood = sc.next();
        
        if(!mood.equals("happy") && !mood.equals("sad")){
            System.err.println("The mood is not valid!");
            return;
        }
        
        if(age <= 25){
            if(mood.equals("happy")){
                System.out.println("don't worry, it's all over once");
            } else {
                System.out.println("heads up!");
            } 
        } else if(age >= 26 && age <= 50){
            if(mood.equals("happy")){
                System.out.println("really?");
            } else {
                System.out.println("classic mood for this age");
            }
        } else if(age >= 50){
            if(mood.equals("happy") || mood.equals("sad")){
                System.out.println("it doesn't matter anyway");
            }
        }
        sc.close();
    }
}
