package cz.vsb.ekf.jam.three;

import java.util.Random;

public class Three {

    public static void main(String[] args) {
        int numberOfDice = 6;

        Random ranNum = new Random();

        System.out.println("Game started!");
        int total;
        int randomNumber;
        int tryCounter = 0;
        do {
            total = 0;
            for (int i = 0; i < numberOfDice; i++) {
                randomNumber = ranNum.nextInt(6) + 1;
                total = total + randomNumber;
            }
            tryCounter++;
        } while (total != 19);

        System.out.println("try counter: " + tryCounter);
        System.out.println("Total: " + total);
    }

}
