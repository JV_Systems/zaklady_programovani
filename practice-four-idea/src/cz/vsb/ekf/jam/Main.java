package cz.vsb.ekf.jam;

import cz.vsb.ekf.jam.model.Sex;
import cz.vsb.ekf.jam.model.User;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        User user1 = new User(1, "David", "Jamarik", "SlepiciBujon23", "MojeHeslo123", 25, Sex.MALE, new BigDecimal("1500"));
        User user2 = new User(2, "Cristiano", "Ronaldo", "Cristi23", "MojeHeslo123", 25, Sex.TRANS, new BigDecimal("2500"));
        User user3 = new User(3, "Bara", "Votikova", "Barca23", "MojeHeslo123", 25, Sex.FEMALE, new BigDecimal("3500"));
        User user4 = new User(4, "Lionel", "Messi", "Leo23", "MojeHeslo123", 25, Sex.MALE, new BigDecimal("4500"));
        User user5 = new User(5, "Michal", "Krmencik", "Majkl0", "MojeHeslo123", 25, Sex.OTHER, new BigDecimal("5500"));
        User[] users = {user1,user2,user3, user4, user5};

        System.out.println("===Welcome here===");
        System.out.println("Set your username: ");
        String username = sc.next();

        System.out.println("Set your password: ");
        String password = sc.next();

        User loggedUser = validateAllUsers(users, username, password);
        if(loggedUser == null){
            System.out.println("Invalid username or password");
            return;
        }
        System.out.println("===");
        System.out.println("===");
        System.out.println("===User dashboard===\n" + loggedUser);
        System.out.println("Please set price to pay: ");
        System.out.println("===");
        System.out.println("===");
        double price = sc.nextDouble();

        if(new BigDecimal(price).compareTo(loggedUser.getPriceToPay()) == 1){
            System.err.println("The price is bigger than bill");
            return;
        } else if(new BigDecimal(price).compareTo(loggedUser.getPriceToPay()) == 0){
            System.out.println("the bill has been paid");
            return;
        } else {
            loggedUser.setPriceToPay(loggedUser.getPriceToPay().subtract(new BigDecimal(price)));
            System.out.println("remaining amount to be paid => " + loggedUser.getPriceToPay() + " <= ");
            return;
        }
    }

    private static User validateAllUsers(User[] users, String username, String password){
        for(User u : users){
            if(u.validateInputValues(username, password)){
                System.out.println("Successfully logged as " + u.getName() + " " + u.getSurname());
                return u;
            }
        }
        return null;
    }
}
