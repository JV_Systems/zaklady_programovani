package cz.vsb.ekf.jam.quiz;

import java.util.Arrays;
import java.util.Comparator;

public class Quiz1 {

    public static void main(String args[]) {
        String[] ar = {"c","d","b","a","e"};

        InnerClass in=new InnerClass();
        Arrays.parallelSort(ar, in);
        //Arrays.sort(ar);

        for(String str : ar) System.out.println(str + "");
        System.out.println(Arrays.binarySearch(ar, "b"));
    }

    static class InnerClass implements Comparator<String>
    {
        public int compare(String s1, String s2)
        {
            return s2.compareTo(s1);
        }
    }

}

//output?? - /A. "e d c b a -1" /B. "a b c 0 e d" /C. " d b c e a 1' /D.  "e b a d c"
