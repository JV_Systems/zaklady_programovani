package cz.vsb.ekf.jam.model;

import java.math.BigDecimal;

public class User {
    private Integer id;
    private String name;
    private String surname;
    private String username;
    private String password;
    private Integer age;
    private Sex sex;
    private BigDecimal priceToPay;

    public User(Integer id, String name, String surname, String username, String password, Integer age, Sex sex, BigDecimal priceToPay) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
        this.age = age;
        this.sex = sex;
        this.priceToPay = priceToPay;
    }

    public User(Integer id, String name, String surname, String username, String password) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
        this.priceToPay = new BigDecimal(Math.random() * 1000);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public BigDecimal getPriceToPay() {
        return priceToPay;
    }

    public void setPriceToPay(BigDecimal priceToPay) {
        this.priceToPay = priceToPay;
    }

    @Override
    public String toString() {
        return  "name = " + name + "\n" +
                "surname = " + surname + "\n" +
                "username = " + username + "\n" +
                "price to pay = " + priceToPay + " Kč";
    }

    public boolean validateInputValues(String username, String password){
        if((username != null && !username.equals(this.username)) || (password != null && !password.equals(this.password))){
            return false;
        }
        return true;
    }

    public void substractPriceToPay(BigDecimal price){
        this.setPriceToPay(this.priceToPay != null ? this.getPriceToPay().subtract(price) : null);
    }
    
}
