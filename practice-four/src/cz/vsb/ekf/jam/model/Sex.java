package cz.vsb.ekf.jam.model;

public enum Sex {
    MALE, FEMALE, TRANS, OTHER, ASEXUAL, ANDROGYN, AGENDER, BIGENDER;
}
